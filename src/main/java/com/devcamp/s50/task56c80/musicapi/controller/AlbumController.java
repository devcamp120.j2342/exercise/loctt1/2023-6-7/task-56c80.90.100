package com.devcamp.s50.task56c80.musicapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c80.musicapi.models.Album;
import com.devcamp.s50.task56c80.musicapi.services.AlbumService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class AlbumController {
     @Autowired
     private AlbumService albumService;

     @GetMapping("/albums")
     public ArrayList<Album> albumList(){
          ArrayList<Album> albums = albumService.allAlbums();
          return albums;
     }
}
