package com.devcamp.s50.task56c80.musicapi.models;

public class BrandMember extends Composer{
     private String instrument;

     public BrandMember(String firstName, String lastName, String stageName) {
        super(firstName, lastName, stageName);

    }

    public BrandMember(String firstName, String lastName) {
        super(firstName, lastName);

    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public BrandMember(String firstName, String lastName, String stageName, String instrument) {
        super(firstName, lastName, stageName);
        this.instrument = instrument;
    }

    @Override
    public String toString() {
        return "BrandMember [instrument=" + instrument + super.toString() + "]";
    }

}
