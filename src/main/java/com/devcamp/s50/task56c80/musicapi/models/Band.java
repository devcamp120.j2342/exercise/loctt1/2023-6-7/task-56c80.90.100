package com.devcamp.s50.task56c80.musicapi.models;

import java.util.List;

public class Band {
     private String bandName;
     private List<BrandMember> brandMembers;
     private List<Album> albums;

     public String getBandName() {
        return bandName;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }

    public List<BrandMember> getBrandMembers() {
        return brandMembers;
    }

    public void setBrandMembers(List<BrandMember> brandMembers) {
        this.brandMembers = brandMembers;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    public Band(String bandName, List<BrandMember> brandMembers, List<Album> albums) {
        this.bandName = bandName;
        this.brandMembers = brandMembers;
        this.albums = albums;
    }

    public Band(String bandName) {
        this.bandName = bandName;
    }

    @Override
    public String toString() {
        return "Band [bandName=" + bandName + ", brandMembers=" + brandMembers + ", albums=" + albums + "]";
    }
}
