package com.devcamp.s50.task56c80.musicapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c80.musicapi.models.Artist;
import com.devcamp.s50.task56c80.musicapi.models.Composer;

@Service
public class ArtistService {
     @Autowired
     private ComposerService composerService;

     public ArrayList<Artist> allArtists(){
          ArrayList<Composer> composers = composerService.allComposers();
          ArrayList<Artist> artists = new ArrayList<>();
          for(Composer composer : composers){
               if(composer instanceof Artist){
                    artists.add((Artist) composer);
               }
          }
          return artists;
     }
}
