package com.devcamp.s50.task56c80.musicapi.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c80.musicapi.models.Album;

@Service
public class AlbumService {
     Album album1 = new Album("Album 1");
     Album album2 = new Album("Album 2");
     Album album3 = new Album("Album 3");
     Album album4 = new Album("Album 4");
     Album album5 = new Album("Album 5");
     Album album6 = new Album("Album 6");
     Album album7 = new Album("Album 7");
     Album album8 = new Album("Album 8");
     Album album9 = new Album("Album 9");
     Album album10 = new Album("Album 10");

     List<String> songs1 = new ArrayList<>();
     List<String> songs2 = new ArrayList<>();
     List<String> songs3 = new ArrayList<>();
     List<String> songs4= new ArrayList<>();
     List<String> songs5 = new ArrayList<>();
     List<String> songs6 = new ArrayList<>();
     List<String> songs7 = new ArrayList<>();
     List<String> songs8 = new ArrayList<>();
     List<String> songs9 = new ArrayList<>();
     List<String> songs10 = new ArrayList<>();
     
     public ArrayList<Album> album1s(){
          songs1.add("Hong Nhan");
          songs2.add("Song Gio");
          album1.setSongs(songs1);
          album2.setSongs(songs2);
          ArrayList<Album> albums = new ArrayList<>();
          albums.add(album1);
          albums.add(album2);
          return albums;
     }

      public ArrayList<Album> album2s(){
          songs3.add("Nguoi Dan Ba Hoa Da");
          songs3.add("Phao Hong");
          songs4.add("Hoa Cuoi");
          songs4.add("Hao Khi Viet Nam");
          songs5.add("An Com Chua");
          songs5.add("Sang Mat Chua");
          songs6.add("Bon Chu Lam");
          songs6.add("Dong Mau Lac Hong");
          album3.setSongs(songs3);
          album4.setSongs(songs4);
           album5.setSongs(songs5);
            album6.setSongs(songs6);
          ArrayList<Album> albums = new ArrayList<>();
          albums.add(album3);
          albums.add(album4);
          albums.add(album5);
          albums.add(album6);
          return albums;
     }

       public ArrayList<Album> album3s() {
        songs7.add("Empty Cups");
        songs8.add("Suffer");
        songs9.add("River");
        songs10.add("Some Type of Love");
        album7.setSongs(songs7);
        album8.setSongs(songs8);
        album9.setSongs(songs9);
        album10.setSongs(songs10);
        ArrayList<Album> albums = new ArrayList<>();

        albums.add(album7);
        albums.add(album8);
        albums.add(album9);
        albums.add(album10);
        return albums;
    }

     public ArrayList<Album> allAlbums() {
        ArrayList<Album> albums = new ArrayList<>();
        albums.addAll(album1s());
        albums.addAll(album2s());
        albums.addAll(album3s());
        return albums;

    }

}
