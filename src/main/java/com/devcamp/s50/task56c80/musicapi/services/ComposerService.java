package com.devcamp.s50.task56c80.musicapi.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s50.task56c80.musicapi.models.Artist;
import com.devcamp.s50.task56c80.musicapi.models.Band;
import com.devcamp.s50.task56c80.musicapi.models.BrandMember;
import com.devcamp.s50.task56c80.musicapi.models.Composer;

@Service
public class ComposerService {
     @Autowired
     private AlbumService albumService;
     Artist artist1 = new Artist("Truong Tan ", "Loc", "Sin Sin");
     Artist artist2 = new Artist("Vuong Thi My ", "Cam", "Sun");
     Artist artist3 = new Artist("Truong Phat ", "Tai", "Bu Truong");

     BrandMember brandMember1 = new BrandMember("Band", "EDM-1", "edm1", "drum");
     BrandMember brandMember2 = new BrandMember("Band", "EDM-2", "edm2", "drum");
     BrandMember brandMember3 = new BrandMember("Band", "EDM-3", "edm3", "drum");
     BrandMember brandMember4 = new BrandMember("Band", "EDM-4", "edm4", "drum");
     BrandMember brandMember5 = new BrandMember("Band", "EDM-5", "edm5", "drum");
     BrandMember brandMember6 = new BrandMember("Band", "EDM-6", "edm6", "drum");

     public ArrayList<Composer> allComposers() {
        artist1.setAlbums(albumService.album1s());
        artist2.setAlbums(albumService.album2s());
        artist3.setAlbums(albumService.album3s());
        ArrayList<Composer> composers = new ArrayList<>();
        composers.add(brandMember1);
        composers.add(brandMember2);
        composers.add(brandMember3);
        composers.add(brandMember4);
        composers.add(brandMember5);
        composers.add(brandMember6);
        composers.add(artist1);
        composers.add(artist2);
        composers.add(artist3);
        return composers;

    }
     public ArrayList<Band> allBands() {
        Band band1 = new Band("Drum1");
        Band band2 = new Band("Drum2");
        ArrayList<BrandMember> brandMembers = new ArrayList<>();
        brandMembers.add(brandMember1);
        brandMembers.add(brandMember2);
        brandMembers.add(brandMember3);
        ArrayList<BrandMember> brandMembersGr1 = new ArrayList<>();
        brandMembersGr1.add(brandMember4);
        brandMembersGr1.add(brandMember5);
        brandMembersGr1.add(brandMember6);

        band1.setBrandMembers(brandMembersGr1);
        band2.setBrandMembers(brandMembers);
        band1.setAlbums(albumService.album1s());
        band2.setAlbums(albumService.album2s());
        ArrayList<Band> bands = new ArrayList<>();
        bands.add(band2);
        bands.add(band1);
        return bands;

    }
}
