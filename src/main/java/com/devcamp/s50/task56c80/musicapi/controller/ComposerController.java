package com.devcamp.s50.task56c80.musicapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task56c80.musicapi.models.Artist;
import com.devcamp.s50.task56c80.musicapi.models.Band;
import com.devcamp.s50.task56c80.musicapi.services.ArtistService;
import com.devcamp.s50.task56c80.musicapi.services.ComposerService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ComposerController {
     @Autowired
     private ComposerService composerService;

     @GetMapping("/bands")
     public ArrayList<Band> bandList(){
          ArrayList<Band> bands = composerService.allBands();
          return bands;
     }

     @Autowired
     private ArtistService artistService;

     @GetMapping("/artists")
     public ArrayList<Artist> artistList(){
          ArrayList<Artist> artists = artistService.allArtists();
          return artists;
     }
}
